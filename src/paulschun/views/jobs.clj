(ns paulschun.views.jobs
  (:require [paulschun.layout :as layout]))

(defn jobs []
  (layout/base-layout
   {:title "Jobs"
    :description "Jobs that Paul S. Chun can set you up with"
    :content
    [:div.container
     [:section.section.hero.psc-content
      [:h3.psc-content__title "Jobs From Companies I Know"]
      [:p "Below is a list of jobs from companies I'm well acquainted with. If we know each other, let me know and I'll see if I can get you a personal intro."]
      [:p.is-italic "(Note: That is entirely fictional and I don't know any of these companies)"]
      [:p " "]
      [:div
       [:div#elbower-app]
       [:script
        {:src
         "https://embed.elbower.com/iframe-integration/js/unorganized-space"}]]
      [:p
       "Powered by "
       [:a {:href "https://elbower.com/", :target "_blank"} "Elbower"]]]]}))
