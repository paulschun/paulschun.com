(ns paulschun.views.about
  (:require [paulschun.layout :as layout]))

(defn about []
  (layout/base-layout
   {:title "About Me"
    :description "All about Paul S. Chun: his background, mission and motivations to focus on understanding and spreading knowledge on human habit formation."
    :content
    [:div.container
     [:section.section.hero.psc-content
             [:h3.psc-content__title "About Me"]
       [:p "If you'd like to contact me for professional reasons or even if you just want to say hi, please use the following form to get in touch."]
      ]]}))
