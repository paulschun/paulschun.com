(ns paulschun.routes
  (:require [paulschun.views.about :refer [about]]
            [paulschun.views.index :refer [index]]
            [paulschun.views.contact :refer [contact]]
            [paulschun.views.jobs :refer [jobs]]
            [clojure.data.xml :as xml]))

(def routes
  (->> [["/" (index) 1.0]
        #_["/about/" (about) 0.5]
        ["/jobs/" (jobs) 0.5]
        ["/contact/" (contact) 0.5]]
       (map #(zipmap [:path :output :weight] %))))

(def robots-txt
  "User-agent: *\nDisallow:\nSitemap: https://paulschun.com/sitemap.xml")

(defn pagerow->sitemap-url [{:keys [path weight]}]
  [:url
   [:loc (str "https://paulschun.com" path)]
   [:priority weight]])

(defn generate-sitemap [routes]
  (xml/emit-str
   (xml/sexp-as-element
    [:urlset
     {:xmlns "http://www.sitemaps.org/schemas/sitemap/0.9"}
     (map pagerow->sitemap-url routes)])))

(defn pages []
  (merge
   {"/sitemap.xml" (generate-sitemap routes)
    "/robots.txt" robots-txt}
   (->> routes
        (map #(vector (:path %) (:output %)))
        (into {}))))
