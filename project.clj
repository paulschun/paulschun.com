(defproject paulschun "0.1.0-SNAPSHOT"
  :description "Paul S. Chun's personal website."
  :url "https://paulschun.com/"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[garden "1.3.10"]
                 [hiccup "1.0.5"]
                 [image-resizer "0.1.10"]
                 [optimus "0.20.2"]
                 [org.clojure/clojure "1.8.0"]
                 [ring "1.8.1"]
                 [rum "0.11.4"]
                 [stasis/stasis "2.5.0"]
                 [org.clojure/data.xml "0.0.8"]]
  :plugins [[lein-garden "0.3.0"]
            [lein-ring "0.12.5"]]
  :ring {:handler paulschun.core/app}
  :aliases {"build-favicons" ["run" "-m" "paulschun.favicon/export"]
            "build-site" ["run" "-m" "paulschun.core/export"]}
  :garden {:builds [{:id "all"
                     :source-paths ["src"]
                     :stylesheet paulschun.styles/main
                     :compiler {:output-to "resources/public/styles/all.css"
                                :pretty-print? false}}]})
