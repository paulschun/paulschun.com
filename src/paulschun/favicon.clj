(ns paulschun.favicon
  (:require
   [clojure.java.io :refer [file]]
   [image-resizer.core :as resizer]
   [image-resizer.format :as format]))

(def favicon-sizes
  [{:prefix "apple-icon-"
    :sizes [57 60 72 76 114 120 144 152 160]}
   {:prefix "android-icon-"
    :sizes [192]}
   {:prefix "favicon-"
    :sizes [32 96 16]}])

(def base-image
  "resources/public/images/favicon/paulschun-favicon-base.png")

(defn duplicate-image [base-image prefix dimension]
  (let [filename (str prefix dimension "x" dimension ".png")]
    (format/as-file
     (resizer/resize base-image dimension dimension)
     (format "resources/public/images/favicon/%s" filename)
     :verbatim)))

(defn export []
  (doseq [{:keys [prefix sizes]} favicon-sizes]
    (doseq [size sizes]
      (duplicate-image (file base-image) prefix size))))
